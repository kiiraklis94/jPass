![jPlanner Repo](http://i.imgur.com/mJWOear.png)

# README #

This is a fork of jPlanner (my other project). Since most of the code could be rupurposed, I did just that.

This application is for managing your logins and passwords for websites (maybe other stuff too, in the future).

**NOTE:** This app is **NOT** secure. There is no encryption or any protection yet. So anyone who can access your computer, can access your added passwords. I don't recommend using it yet. 

Having said that, the application WILL get these features. Both encryption and some kind of password protection (master password) to access it. Stay tuned and frequently check the Downloads page. The application will be packaged and uploaded there when everything is complete.

# About the download #

This is the first actual release of the application in executable .jar format.

Again, I DO NOT reccommend you use it for storing actual passwords. The application has no connection to the internet and runs locally only, but the database file is NOT encrypted so some users might see it as a security flaw (and it is).

I'm looking into ways to make it more secure. But you can experiment with it and see if you like it. There is password protection for the app, but the master password can be easily restored (if you open the jar with WinRAR for example, the file "main.db" contains all the information).

Be careful.