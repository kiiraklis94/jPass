import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DeleteFromDB {
	
	Connection conn = null;
	Statement stmt = null;
	
	public void Delete(String db, int id){
		
		switch (db){
		
		case "WebPasswords" :
			try {
				
				conn = dbConnection.dbConnector();
				stmt = conn.createStatement();
				
				String sql = "DELETE FROM WebPasswords WHERE ROWID=" +id +"; VACUUM;";
				
				System.out.println(sql);
				
				stmt.executeUpdate(sql);
				stmt.close();
				conn.close();
				System.out.println("Row deleted successfully");
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
			
		case "EmailPasswords" :
			try {
				
				conn = dbConnection.dbConnector();
				stmt = conn.createStatement();
				
				String sql = "DELETE FROM EmailPasswords WHERE ROWID=" +id +"; VACUUM;";
				
				System.out.println(sql);
				
				stmt.executeUpdate(sql);
				stmt.close();
				conn.close();
				System.out.println("Row deleted successfully");
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
		}
	}
}
