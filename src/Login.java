import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;

@SuppressWarnings("serial")
public class Login extends JDialog {

	private final JPanel loginPanel = new JPanel();
	private final JPanel createPanel = new JPanel();
	private JPasswordField masterPasswordField;
	private JPasswordField createPasswordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		try {
			Login dialog = new Login();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Login() {
		
		getContentPane().setFont(new Font("Verdana", Font.PLAIN, 14));
		setAlwaysOnTop(true);
		setResizable(false);
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setFont(new Font("Dialog", Font.PLAIN, 14));
		setTitle("Login to jPass");
		setBounds(100, 100, 326, 147);
		getContentPane().setLayout(new CardLayout(0, 0));
		
			loginPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
			getContentPane().add(loginPanel, "name_21220960140813");
			loginPanel.setLayout(null);

			JLabel lblEnterMasterPassword = new JLabel("Enter Master Password:");
			lblEnterMasterPassword.setBounds(10, 11, 300, 18);
			loginPanel.add(lblEnterMasterPassword);
			lblEnterMasterPassword.setVerticalAlignment(SwingConstants.BOTTOM);
			lblEnterMasterPassword.setToolTipText("");
			lblEnterMasterPassword.setFont(new Font("Verdana", Font.BOLD, 14));
			
			masterPasswordField = new JPasswordField();
			masterPasswordField.setBounds(10, 34, 300, 35);
			loginPanel.add(masterPasswordField);
			masterPasswordField.setFont(new Font("Verdana", Font.PLAIN, 14));
			masterPasswordField.setToolTipText("Enter the master password you have specified");
			
			JButton loginOKButton = new JButton("OK");
			loginOKButton.setBounds(10, 75, 148, 35);
			loginPanel.add(loginOKButton);
			loginOKButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					String pass = String.valueOf(masterPasswordField.getPassword()); //Convert password to string instead of char[]
					if(CheckIfPassExists()){
						if (!CheckPass(pass)){ //if password is NOT correct
							masterPasswordField.setText("");
							JOptionPane.showMessageDialog(loginPanel, "Wrong password", "Error", JOptionPane.ERROR_MESSAGE);
							
						}else{           //if password IS correct
							masterPasswordField.setText("");
							dispose();
							new MainWindow().mainFrame.setVisible(true);
							}
					}else{
						masterPasswordField.setText("");
						loginPanel.setVisible(false);
						createPanel.setVisible(true);
						}
				}
			});
			loginOKButton.setFont(new Font("Verdana", Font.BOLD, 16));
			
			JButton changePassButton = new JButton("Create new");
			changePassButton.setToolTipText("If you don't have a password, or if you want to make a new one, click here.");
			changePassButton.setFont(new Font("Verdana", Font.BOLD, 16));
			changePassButton.setBounds(162, 75, 148, 35);
			loginPanel.add(changePassButton);
			changePassButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					String pass = String.valueOf(masterPasswordField.getPassword());
					
					if(CheckIfPassExists()){
						if (!CheckPass(pass)){ //if password is NOT correct
							masterPasswordField.setText("");
							JOptionPane.showMessageDialog(loginPanel, "Wrong password", "Error", JOptionPane.ERROR_MESSAGE);
							
							}
						else{           //if password IS correct
							masterPasswordField.setText("");
							loginPanel.setVisible(false);
							createPanel.setVisible(true);
							}
					}else{
						masterPasswordField.setText("");
						loginPanel.setVisible(false);
						createPanel.setVisible(true);
					}
				}
			});


			createPanel.setLayout(null);
			createPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
			getContentPane().add(createPanel, "name_21408414966217");
			
			JLabel lblCreateMasterPassword = new JLabel("Create Master Password:");
			lblCreateMasterPassword.setVerticalAlignment(SwingConstants.BOTTOM);
			lblCreateMasterPassword.setToolTipText("");
			lblCreateMasterPassword.setFont(new Font("Verdana", Font.BOLD, 14));
			lblCreateMasterPassword.setBounds(10, 11, 300, 18);
			createPanel.add(lblCreateMasterPassword);
			
			createPasswordField = new JPasswordField();
			createPasswordField.setToolTipText("Enter the master password you have specified");
			createPasswordField.setFont(new Font("Verdana", Font.PLAIN, 14));
			createPasswordField.setBounds(10, 34, 300, 35);
			createPanel.add(createPasswordField);
			
			JButton createOKButton = new JButton("Create");
			createOKButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					String pass = String.valueOf(createPasswordField.getPassword());
					
					if (pass.equals("")){
						JOptionPane.showMessageDialog(createPanel, "Can't use empty password", "Error", JOptionPane.ERROR_MESSAGE);
					}
					else{
						CreatePass(pass);
						createPasswordField.setText("");
						dispose();
						new MainWindow().mainFrame.setVisible(true);
					}
				}
			});
			createOKButton.setFont(new Font("Verdana", Font.BOLD, 16));
			createOKButton.setBounds(10, 75, 300, 35);
			createPanel.add(createOKButton);
	}
	
	public boolean CheckPass(String passwordToCheck){
		
		boolean passwordMatch = false;
		String pass = null;
		Statement stmt = null;
		Connection conn =	null;
		
		try {
			conn = dbConnection.dbConnector();
			stmt = conn.createStatement();
			
			String sql = "SELECT MasterPassword FROM jPassMasterPass;";
			ResultSet rs = stmt.executeQuery(sql);
			
			if(rs.next()){
				pass = rs.getString("MasterPassword");
				}
			else{
				passwordMatch = false;
				}
			rs.close();
			stmt.close();
			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (passwordToCheck.equals(pass) && pass != null){
			passwordMatch = true;
			}

		return passwordMatch;	
	}
	
	public boolean CheckIfPassExists(){
		boolean exists = false;
		String pass = null;
		Connection conn = null;
		
		try {
			
			conn = dbConnection.dbConnector();
			Statement stmt = conn.createStatement();
			String sql = "SELECT MasterPassword FROM jPassMasterPass;";
			ResultSet rs = stmt.executeQuery(sql);
			
			if(rs.next()){
				pass = rs.getString("MasterPassword");
				}
			else{
				pass = null;
				JOptionPane.showMessageDialog(loginPanel, "There is no password set. Please create new master password.", "Error", JOptionPane.ERROR_MESSAGE);

				}

			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			}
			
		if (pass != null){
			exists = true;
			}
		else{
			exists = false;
			}
		return exists;
	}
	
	public void CreatePass(String pass){
		Connection conn = null;
		
		try{
			
			conn = dbConnection.dbConnector();
			Statement stmt = conn.createStatement();
			
			String sql = "DELETE FROM jPassMasterPass;";
			stmt.executeUpdate(sql);
				
			sql = "INSERT INTO jPassMasterPass (MasterPassword) VALUES ('" +pass +"');";
			stmt.executeUpdate(sql);
			
			stmt.close();
			conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
